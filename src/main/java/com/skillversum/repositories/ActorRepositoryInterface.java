package com.skillversum.repositories;

import com.skillversum.entities.Actor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ActorRepositoryInterface extends MongoRepository<Actor, String> {
    Actor findByName(String name);
}
