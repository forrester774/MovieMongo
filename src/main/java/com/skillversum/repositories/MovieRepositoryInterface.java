package com.skillversum.repositories;

import com.skillversum.entities.MovieEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepositoryInterface extends MongoRepository<MovieEntity,String> {

}
