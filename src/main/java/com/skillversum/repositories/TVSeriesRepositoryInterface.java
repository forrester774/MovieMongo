package com.skillversum.repositories;

import com.skillversum.entities.TVSeriesEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TVSeriesRepositoryInterface extends MongoRepository<TVSeriesEntity, String> {
    TVSeriesEntity findByTitle(String title);
}
