package com.skillversum.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
public class LoginBean {
    public void onPageLoad(){
        //Egy új auth objektumba betölti az adott user authentikációját
        //instanceof egy type comparator keyword, tehát hogy egy bizonyos objektum egy adott classhoz tartozik-e
        //itt azt nézi meg, hogy az auth objektum az AnonymousAuthenticationToken classhoz tartozik-e
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            try {
                //Ha be van jelentkezve a user(tehát auth nem Anonymus), akkor átirányítja a megadott weboldalra.
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/index.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
