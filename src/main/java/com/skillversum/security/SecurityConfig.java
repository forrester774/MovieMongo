package com.skillversum.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//WebSecurityConfigurerAdapter class egy olyan class, ami az alapvető security configot szoljáltatja
//Az @EnableWebSecurity annotáció szó szerint azt csinálja ami a neve, engedélyezi hogy ez a class kezelje a spring
// security beállításait.
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //Ez a method megmondja, hogy hogyan legyen autentikálva a felhasználó
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // require all requests to be authenticated except for the resources(css, js)
        http.authorizeRequests()
                .antMatchers("/javax.faces.resource/**","/tvdb.xhtml").permitAll().anyRequest()
                .authenticated();

        //login - rákötjük a bejelentkezést a login.xhtml-re, ha nem sikerül, akkor dobjon vissza a login.xhtml-re
        // egy error felirattal
        //Sikeres belépés esetén az index.xhtml-re dob át
        http.formLogin().loginPage("/login.xhtml").permitAll().failureUrl("/login.xhtml?error=true")
                .successForwardUrl("/index.xhtml");

        //logout - kijelentkezés és hogy hova jelentkezzünk ki.
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.xhtml");

        //CSRF támadás elleni védelem kikapcsolása mert jsf alapból véd ellene
        http.csrf().disable();


    }

    //két teszt user hozzáadása
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication().withUser("User1")
                .password("1234").roles("USER").and().withUser("User2")
                .password("5678").roles("ADMIN");
    }
}
