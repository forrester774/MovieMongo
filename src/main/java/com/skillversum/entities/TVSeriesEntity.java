package com.skillversum.entities;

import java.io.Serializable;
import java.util.Date;


public class TVSeriesEntity extends AbstractAnimation implements Serializable {

    private Integer seasons;

    public TVSeriesEntity() {

    }

    public TVSeriesEntity(String title, Date releaseDate, Integer rating, Integer seasons) {
        super(title, releaseDate, rating);
        this.seasons = seasons;
    }

    public Integer getSeasons() {
        return seasons;
    }

    public void setSeasons(Integer seasons) {
        this.seasons = seasons;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return this.getTitle().equals(((TVSeriesEntity) obj).getTitle());
    }
}
