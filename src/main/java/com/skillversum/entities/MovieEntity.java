package com.skillversum.entities;

import java.util.Date;


public class MovieEntity extends AbstractAnimation{


    private Actor actor;

    private String sequel;

    private TVSeriesEntity seriesForm;

    public MovieEntity() {

    }

    public MovieEntity(String title, Date releaseDate, Integer rating, Actor actor, String sequel, TVSeriesEntity
            seriesForm) {
        super(title, releaseDate, rating);
        this.actor = actor;
        this.sequel = sequel;
        this.seriesForm = seriesForm;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public String getSequel() {
        return sequel;
    }

    public void setSequel(String sequel) {
        this.sequel = sequel;
    }

    public TVSeriesEntity getSeriesForm() {
        return seriesForm;
    }

    public void setSeriesForm(TVSeriesEntity seriesForm) {
        this.seriesForm = seriesForm;
    }
}
